module ApplicationHelper

  # Returns the full title on a per-page basis.  # Documentation Comment
  def full_title(page_title = '')                # Method def, optional arg
    base_title = "KADOSILANG"                     # Variable assignment
    if page_title.empty?                         # Booloean test
      base_title                                 # Implicit return
    else
      page_title + " | " + base_title            # String concatenation
    end
  end
end
