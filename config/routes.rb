Rails.application.routes.draw do
  root 'pages#home'
  get '/give', to: 'pages#give'
  get '/receive', to: 'pages#receive'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
