require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get root_path
    assert_response :success
  end
  
  test "should get give" do
    get give_url
    assert_response :success
  end

  test "should get receive" do
    get receive_url
    assert_response :success
  end

end
